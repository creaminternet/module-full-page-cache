<?php

class Cream_Fpc_Helper_Config  extends Mage_Core_Helper_Abstract
{
	const XML_PATH_CACHEABLE_ACTIONS	= 'cream_fpc/general/cache_actions';
	const XML_PATH_DYNAMIC_BLOCKS		= 'cream_fpc/general/dynamic_blocks';
	const XML_PATH_SESSION_BLOCKS		= 'cream_fpc/general/session_blocks';
	const XML_PATH_REFRESH_ACTIONS		= 'cream_fpc/general/refresh_actions';	
	const XML_PATH_SESSION_PARAMS		= 'cream_fpc/general/session_params';

	/**
	 * Returns an array with refresh actions.
	 *
	 * @return array
	 */
	public function getRefreshActions()
	{
		return $this->_getStoreConfig(self::XML_PATH_REFRESH_ACTIONS);
	}	
	
	/**
	 * Returns an array with cacheable actions.
	 * 
	 * @return array
	 */
	public function getCacheableActions()
	{
		return $this->_getStoreConfig(self::XML_PATH_CACHEABLE_ACTIONS);
	}
	
	/**
	 * Returns an array with all dynamic blocks.
	 * 
	 * @return array
	 */
	public function getDynamicBlocks()
	{
		return $this->_getStoreConfig(self::XML_PATH_DYNAMIC_BLOCKS);
	}
	
	/**
	 * Returns an array with all the session blocks.
	 * 
	 * @return array
	 */
	public function getSessionBlocks()
	{
		return $this->_getStoreConfig(self::XML_PATH_SESSION_BLOCKS);
	}
	
	/**
	 * Returns an array with all session params.
	 *
	 * @return array
	 */
	public function getSessionParams()
	{
		return $this->_getStoreConfig(self::XML_PATH_SESSION_PARAMS);
	}	

	/**
	 * Returns an array with all dynamic blocks
	 * 
	 * @return array
	 */
	public function getAllBlocks()
	{
		return array_merge($this->getDynamicBlocks(), $this->getSessionBlocks());	
	}
	
    /**
     * Returns an array from the comma seperated config
     *
     * @param $path
     * @return array
     */
    protected function _getStoreConfig($path)
    {
        $configs = trim(Mage::getStoreConfig($path));

        if ($configs) {
            return array_unique(array_map('trim', explode(',', $configs)));
        }

        return array();
    }
}