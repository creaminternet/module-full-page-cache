<?php

class Cream_Fpc_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Constant holding layout element class.
     *
     * @var string
     */
    const LAYOUT_ELEMENT_CLASS = 'Mage_Core_Model_Layout_Element';

    /**
     * Returns the placeholder for the given block name
     *
     * @param string $name
     * @return string
     */
    public function getPlaceholder($name)
    {
        return sprintf('<!--fpc_%s-->', $name);
    }

    /**
     * Returns the unique cache key for this request.
     *
     * @return string
     */
    public function getCacheKey()
    {
        $design = Mage::getSingleton('core/design_package');
        $customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();

        $cacheKeyElements = array(
            'requestUrl'        => $_SERVER['REQUEST_URI'],
            'hostname'          => $_SERVER['HTTP_HOST'],
            'ssl'               => isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : '',
            'store_id'          => Mage::app()->getStore()->getStoreId(),
            'customer_group_id' => $customerGroupId,
            'currency_code'     => Mage::app()->getStore()->getCurrentCurrency()->getCurrencyCode(),
            'design'            => $design->getPackageName().'_'.$design->getTheme('layout')
        );

        if ($this->getFullActionName() == 'catalog_category_view') {
            $sessionParams = Mage::helper('cream_fpc/config')->getSessionParams();
            $catalogSession = Mage::getSingleton('catalog/session');
            foreach ($sessionParams as $param) {
                if ($data = $catalogSession->getData($param)) {
                    $cacheKeyElements['session_' . $param] = $data;
                }
            }
        }

        $cacheKey = implode(',', $cacheKeyElements);
        $cacheKey = md5($cacheKey);

        return Cream_Fpc_Model_Fpc::CACHE_PREFIX.$cacheKey;
    }

    /**
     * Returns a unique hash for the session blocks.
     *
     * @return string
     */
    public function getSessionKey()
    {
        $design = Mage::getSingleton('core/design_package');
        $customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();

        $sessionKeyElements = array(
            'hostname'          => $_SERVER['HTTP_HOST'],
            'ssl'               => isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : '',
            'store_id'          => Mage::app()->getStore()->getStoreId(),
            'customer_group_id' => $customerGroupId,
            'currency_code'     => Mage::app()->getStore()->getCurrentCurrency()->getCurrencyCode(),
            'design'            => $design->getPackageName().'_'.$design->getTheme('layout')
        );

        $sessionKey = implode(',', $sessionKeyElements);
        return md5($sessionKey);
    }

    /**
     * Returns true if the request is cacheable, otherwise false.
     *
     * @return boolean
     */
    public function isCacheable()
    {
        // Only cache get requests
        if (!Mage::app()->getRequest()->isGet()) {
            return false;
        }

        // Don't cache ajax requests.
        if (Mage::app()->getRequest()->isXmlHttpRequest() || Mage::app()->getRequest()->getParam('isAjax')) {
            return false;
        }

        // Check config for cachable actions.
        if (!in_array($this->getFullActionName(), Mage::helper('cream_fpc/config')->getCacheableActions())) {
            return false;
        }

        // If no cache param is set do not cache
        if (Mage::app()->getRequest()->has('no_cache')) {
            return false;
        }

        // Only cache pages with status code 200
        if (Mage::app()->getResponse()->getHttpResponseCode() != 200) {
            return false;
        }

        return true;
    }

    /**
     * Returns the full action name.
     *
     * @return string
     */
    public function getFullActionName()
    {
        $delimiter = '_';
        $request = Mage::app()->getRequest();
        return $request->getRequestedRouteName() . $delimiter .
        $request->getRequestedControllerName() . $delimiter .
        $request->getRequestedActionName();
    }

    /**
     * Determin if the session blocks are still valid.
     *
     * @return boolean
     */
    public function isSessionBlockValid()
    {
        $session = Mage::getSingleton('customer/session');

        if ($session->getSessionKey() === false || $session->getSessionKey() != $this->getSessionKey()) {
            $session->setSessionKey($this->getSessionKey());
            return false;
        }
        return true;
    }
}
