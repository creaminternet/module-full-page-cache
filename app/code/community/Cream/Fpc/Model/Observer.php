<?php

class Cream_Fpc_Model_Observer
{
    /**
     * @return Cream_Fpc_Model_Fpc
     */
    protected function _getFpc()
    {
        return Mage::getSingleton('cream_fpc/fpc');
    }
    
    /**
     * Refreshes the cache based on the given action name and 
     * refresh actions in the config.
     * 
     * @param Mage_Core_Model_Observer $observer
     */
    public function refreshCache($observer) 
    {
    	$session = Mage::getSingleton('customer/session');
    	$refreshActions = Mage::helper('cream_fpc/config')->getRefreshActions();
    	$fullActionName = Mage::helper('cream_fpc')->getFullActionName();

    	if (in_array($fullActionName, $refreshActions)) {
    		$session->setSessionKey(false);
    	}
    }
	
    /**
     * Loads the cache and writes to the browser
     * 
     * @param Mage_Core_Model_Observer $observer
     */
	public function loadCache($observer)
	{
		$key = Mage::helper('cream_fpc')->getCacheKey();
		
		if ($html = $this->_getFpc()->load($key)) {
			$response = Mage::app()->getResponse();
			$response->setHeader('FPC', true);
			$response->setBody($html);
			$response->sendResponse();	
			exit;
		}
	}	

	/**
	 * Saves the output to the full page cache.
	 * 
	 * @param Mage_Core_Model_Observer $observer
	 */
	public function saveCache($observer)
	{
		if ($this->_getFpc()->isEnabled() && !$this->_getFpc()->isCached() && Mage::helper('cream_fpc')->isCacheable()) {
	
			$key = Mage::helper('cream_fpc')->getCacheKey();			
			$html = $observer->getEvent()->getResponse()->getBody();
			$html = $this->_getFpc()->save($key, $html);
			$observer->getEvent()->getResponse()->setBody($html);
		}
	}
	
	/**
	 * Process the block after rendering, replaces the output with the given
	 * placeholder key. 
	 * 
	 * @param Mage_Core_Model_Observer $observer
	 */
	public function processBlock($observer)
	{
		if ($this->_getFpc()->isEnabled() && !$this->_getFpc()->isCached() && Mage::helper('cream_fpc')->isCacheable()) {
			$this->_getFpc()->replaceBlockWithPlaceholder($observer->getEvent()->getBlock(), $observer->getTransport());			
		}
	}
	
	/**
	 * Cleans the full page cache based 
	 * 
	 * @param Mage_Core_Model_Observer $observer
	 */
	public function processClean($observer)
	{
		$tags = array();
		
		if ($this->_getFpc()->isEnabled()) {
			$object = $observer->getEvent()->getObject();
			if ($object instanceof Mage_Cms_Model_Block) {
				$tags[] = 'cmsblock_'.$object->getIdentifier();
			} elseif ($object instanceof Mage_Cms_Model_Page) {
				$tags[] = 'cms_'. $object->getId();
				$tags[] = 'cms_'. $object->getIdentifier();				
			} elseif ($object instanceof Mage_Catalog_Model_Product) {				
				$tags[] = 'product_'. $object->getId();
			} elseif ($object instanceof Mage_Catalog_Model_Category) {
				$tags[] = 'category_'. $object->getId();	
			} elseif ($object instanceof Mage_Index_Model_Event) {
				$dataObject = $object->getDataObject();
				if ($object->getType() === 'mass_action' && $object->getEntity() === 'catalog_product' && $dataObject instanceof Mage_Catalog_Model_Product_Action) {
					foreach($dataObject->getProductIds() as $productId) {
						$tags[] = 'product_'. $productId;						
					}
				}
			}
			
			if (count($tags)) {
				$this->_getFpc()->clean($tags);
			}
		}		
	}
}