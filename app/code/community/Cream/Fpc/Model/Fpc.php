<?php

class Cream_Fpc_Model_Fpc
{
	const FORM_KEY_PLACEHOLDER = '<!--fpc_form_key_placeholder-->';
	const SESSION_ID_PLACEHOLDER = '<!--fpc_session_id_placeholder-->';
	
	/**
	 * The prefix to attach to every cache key to make it unique.
	 * 
	 * @var string
	 */
	const CACHE_PREFIX = 'fpc_';
	
	/**
	 * Holds if the current requests is cached or not.
	 * 
	 * @var boolean
	 */
	protected $_isCached = false;
	
	/**
	 * Holds the placeholder keys
	 * 
	 * @var array
	 */
	protected $_placeholder = array();
	
	/**
	 * Flag if layout is processed.
	 * 
	 * @var boolean
	 */
	protected $_processedLayout = false;
	
	/**
	 * Holds the HTML for the placeholders.
	 * 
	 * @var array
	 */
	protected $_placeholderHtml = array();
	
	/**
	 * Holds the cache tags for this request.
	 * 
	 * @var array
	 */
	protected $_cacheTags = array('FPC');
		
	/**
	 * Check to see if the page cache is enabled. Returns true if it is, 
	 * otherwise returns false.
	 * 
	 * @return boolean
	 */
	public function isEnabled()
	{
		return Mage::app()->useCache('fpc');
	}	
	
	/**
	 * Returns true if the current request is cached, otherwise false.
	 * 
	 * @return boolean
	 */
	public function isCached()
	{
		return $this->_isCached;
	}
	
	/**
	 * Loads the cache and replaces dynamic content. Returns the cached document
	 * or null when no cache is found.
	 * 
	 * @param string $key
	 * @return string|null
	 */
	public function load($key)
	{
		if ($html = Mage::app()->loadCache($key)) {
			$this->_isCached = true;

			$this->addBlockPlaceholder();
			$this->addFormKeyPlaceholder();
			$this->addSessionIdPlaceholder();

			return str_replace($this->_placeholder, $this->_placeholderHtml, $html);
		}		
	}
	
	/**
	 * Saves the given HTML to the cache and returns a processed one with all
	 * placeholder replaced by the actuel content. 
	 * 
	 * @param string $key
	 * @param string $html
	 * @return string
	 */
	public function save($key, $html)
	{
		$html = $this->replaceFormKeyWithPlaceholder($html);
		$html = $this->replaceSessionIdWithPlaceholder($html);
		
		$this->_addCacheTags(Mage::getModel('cream_fpc/fpc_tags')->getTags());

		Mage::app()->saveCache($html, $key, $this->_cacheTags);

		$this->_isCached = true;
				
		return str_replace($this->_placeholder, $this->_placeholderHtml, $html);
	}
	
	/**
	 * Clean the cache for the given key or array of keys.
	 * 
	 * @param array|string $key
	 */
	public function clean($key)
	{
		if ($this->isEnabled()) {
			Mage::app()->cleanCache($key);
		}
	}
	
	/**
	 * Processes the given block, replaces the HTML by the placeholder when necessary
	 * 
	 * @param Mage_Core_Block_Abstract $block
	 */
	public function replaceBlockWithPlaceholder(Mage_Core_Block_Abstract $block, $transport)
	{
		$session = Mage::getSingleton('customer/session');		
		$blockName = $block->getNameInLayout();
		$allBlocks = Mage::helper('cream_fpc/config')->getAllBlocks();
		$sessionBlocks = Mage::helper('cream_fpc/config')->getSessionBlocks(); 
		
		$this->_addCacheTagsFromBlock($block);
				
		if (in_array($blockName, $allBlocks)) {
			$placeholder = Mage::helper('cream_fpc')->getPlaceholder($blockName);
			$html = $transport->getHtml();
			
			$this->_addPlaceholder($placeholder, $html);
			$transport->setHtml($placeholder);			
		}
	}
	
	/**
	 * Changes the block HTML placeholder to the actual HTML content. 
	 * 
	 */
	public function addBlockPlaceholder()
	{
		$session = Mage::getSingleton('customer/session');
		$allBlocks = Mage::helper('cream_fpc/config')->getAllBlocks();
		$sessionBlocks = Mage::helper('cream_fpc/config')->getSessionBlocks();
		$dynamicBlocks = Mage::helper('cream_fpc/config')->getDynamicBlocks();
		
		if (Mage::helper('cream_fpc')->isSessionBlockValid()) {
			foreach ($sessionBlocks as $blockName) {
				$placeholder = Mage::helper('cream_fpc')->getPlaceholder($blockName);
				$html = $session->getData('fpc_session_block_' . $blockName);
				
				$this->_addPlaceholder($placeholder, $html);				
			}
			
			$processBlocks = $dynamicBlocks;
		} else {
			$processBlocks = $allBlocks;
		}
				
		foreach ($processBlocks as $blockName) {
			$block = $this->_getLayout($processBlocks)->getBlock($blockName);
			if ($block) {

				$placeholder = Mage::helper('cream_fpc')->getPlaceholder($blockName);
				$html = $block->toHtml();
				
				if (in_array($blockName, $sessionBlocks)) {
					$session->setData('fpc_session_block_' . $blockName, $html);
				}				

				$this->_addPlaceholder($placeholder, $html);
			}
		}
	}
	
	/**
	 * Replaces the form key with a placeholder.
	 * 
	 * @param string $html
	 * @return string
	 */
	public function replaceFormKeyWithPlaceholder($html)
	{
		$session = Mage::getSingleton('core/session');
		$formKey = $session->getFormKey();
		if ($formKey) {
			$html = str_replace($formKey, self::FORM_KEY_PLACEHOLDER, $html);
			$this->_addPlaceholder(self::FORM_KEY_PLACEHOLDER, $formKey);
		}
	
		return $html;
	}
	
	/**
	 * Replaces the placeholder with the actueal form key.
	 *  
	 */
	public function addFormKeyPlaceholder()
	{
		$coreSession = Mage::getSingleton('core/session');
		$formKey = $coreSession->getFormKey();
		if ($formKey) {
			$this->_addPlaceholder(self::FORM_KEY_PLACEHOLDER, $formKey);
		}
	}	
	
	/**
	 * Replaced the session id with a placeholder.
	 * 
	 * @param string $html
	 * @return string
	 */
	public function replaceSessionIdWithPlaceholder($html)
	{
		$session = Mage::getSingleton('core/session');
		$sid = $session->getEncryptedSessionId();
		if ($sid) {
			$html = str_replace($sid, self::SESSION_ID_PLACEHOLDER, $html);
			$this->_addPlaceholder(self::SESSION_ID_PLACEHOLDER, $sid);
		}
	
		return $html;
	}	
	
	/**
	 * Adds the session id placeholder.
	 * 
	 */
	public function addSessionIdPlaceholder()
	{
		$this->_addPlaceholder(self::SESSION_ID_PLACEHOLDER, Mage::getSingleton('core/session')->getEncryptedSessionId());		
	}
	
	/**
	 * Adds a placeholder to the cache.
	 * 
	 * @param string $placeholder
	 * @param string $html
	 */
	protected function _addPlaceholder($placeholder, $html)
	{
		$this->_placeholder[] = $placeholder;
		$this->_placeholderHtml[] = $html; 
	}
	
	/**
	 * Adds the given cache tags to the local cache tags storage. 
	 * 
	 * @param array $tags
	 */
	protected function _addCacheTags($cacheTags)
	{
		$this->_cacheTags = array_merge($cacheTags, $this->_cacheTags);
	}
	
	/**
	 * Adds cache tags from a block.
	 * 
	 * @param Mage_Core_Block_Abstract $block
	 */
	protected function _addCacheTagsFromBlock(Mage_Core_Block_Abstract $block)
	{
		$cacheTags = array();
		if ($block instanceof Mage_Catalog_Block_Product_List || is_subclass_of($block, 'Mage_Catalog_Block_Product_List')) {
			$cacheTags[] = 'product';
			foreach ($block->getLoadedProductCollection() as $product) {
				$cacheTags[] = 'product_' . $product->getId();
			}
		} else if ($block instanceof Mage_Cms_Block_Block || is_subclass_of($block, 'Mage_Cms_Block_Block')) {
			$cacheTags[] = 'cmsblock';
			$cacheTags[] = 'cmsblock_' . $block->getBlockId();
		}
		
		$this->_addCacheTags($cacheTags);
	}
	
	/**
	 * Processes the layout blocks
	 * 
	 * @param array $blocks
	 * @return Mage_Core_Model_Layout
	 */
	protected function _getLayout($blocks)
	{
		$layout = Mage::app()->getLayout();
		
		if (!$this->_processedLayout) {
			$xml = simplexml_load_string($layout->getXmlString(), Cream_Fpc_Helper_Data::LAYOUT_ELEMENT_CLASS);
			$cleanXml = simplexml_load_string('<layout/>', Cream_Fpc_Helper_Data::LAYOUT_ELEMENT_CLASS);
			
			$types = array('block', 'reference', 'action');
			foreach ($blocks as $blockName) {
				foreach ($types as $type) {
					$xPath = $xml->xpath("//" . $type . "[@name='" . $blockName . "']");
					foreach ($xPath as $child) {
						$cleanXml->appendChild($child);
					}
				}
			}
			$layout->setXml($cleanXml);
			$layout->generateBlocks();
			$this->_initLayoutMessages($layout);		

			$this->_processedLayout = true;
		}
		
		return $layout;
	}
	
	protected function _initLayoutMessages(Mage_Core_Model_Layout $layout, $messagesStorage = array('catalog/session', 'tag/session', 'checkout/session', 'customer/session'))
	{
		$block = $layout->getMessagesBlock();
		if ($block) {
			foreach ($messagesStorage as $storageName) {
				$storage = Mage::getSingleton($storageName);
				if ($storage) {
					$block->addMessages($storage->getMessages(true));
					$block->setEscapeMessageFlag($storage->getEscapeMessages(true));
				} else {
					Mage::throwException(Mage::helper('core')->__('Invalid messages storage "%s" for layout messages initialization', (string)$storageName));
				}
			}
		}
		return $layout;
	}
	
}