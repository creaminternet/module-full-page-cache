<?php
 
class Cream_Fpc_Model_Fpc_Tags
{
    /**
     * Returns an array of tags.
     * 
     * @return array 
     */
    public function getTags()
    {
        $cacheTags = array();
        switch (Mage::helper('cream_fpc')->getFullActionName()) {
            case 'cms_index_index' :
                $cacheTags = $this->_getCmsIndexIndexCacheTags();
                break;
            case 'cms_page_view' :
                $cacheTags = $this->_getCmsPageViewCacheTags();
                break;
            case 'catalog_product_view' :
                $cacheTags = $this->_getCatalogProductViewCacheTags();
                break;
            case 'catalog_category_view' :
                $cacheTags = $this->_getCatalogCategoryViewCacheTags();
                break;
        }

        return $cacheTags;        
    }

    /**
     * @return array
     */
    protected function _getCmsIndexIndexCacheTags()
    {
        $cacheTags = array();
        $cacheTags[] = 'cms';
        $pageId = Mage::getStoreConfig(Mage_Cms_Helper_Page::XML_PATH_HOME_PAGE);
        
        if ($pageId) {
            $cacheTags[] = 'cms_' . $pageId;
        }
        
        return $cacheTags;
    }

    /**
      * @return array
     */
    protected function _getCmsPageViewCacheTags() 
    {
        $cacheTags = array();
        $cacheTags[] = 'cms';
        $pageId = Mage::app()->getRequest()->getParam('page_id', Mage::app()->getRequest()->getParam('id', false));
        if ($pageId) {
            $cacheTags[] = 'cms_' . $pageId;
        }
        return $cacheTags;
    }

    /**

     * @return array
     */
    protected function _getCatalogProductViewCacheTags()
    {
        $cacheTags = array();
        $cacheTags[] = 'product';
        $productId = (int)Mage::app()->getRequest()->getParam('id');
        if ($productId) {
            $cacheTags[] = 'product_' . $productId;

            // configurable product
            $configurableProduct = Mage::getModel('catalog/product_type_configurable');
            
            // get all childs of this product and add the cache tag
            $childIds = $configurableProduct->getChildrenIds($productId);
            foreach ($childIds as $childIdGroup) {
                foreach ($childIdGroup as $childId) {
                    $cacheTags[] = 'product_' . $childId;
                }
            }
            // get all parents of this product and add the cache tag
            $parentIds = $configurableProduct->getParentIdsByChild($productId);
            foreach ($parentIds as $parentId) {
                $cacheTags[] = 'product_' . $parentId;
            }

            // grouped product
            $groupedProduct = Mage::getModel('catalog/product_type_grouped');
            // get all childs of this product and add the cache tag
            $childIds = $groupedProduct->getChildrenIds($productId);
            foreach ($childIds as $childIdGroup) {
                foreach ($childIdGroup as $childId) {
                    $cacheTags[] = 'product_' . $childId;
                }
            }
            // get all parents of this product and add the cache tag
            $parentIds = $groupedProduct->getParentIdsByChild($productId);
            foreach ($parentIds as $parentId) {
                $cacheTags[] = 'product_' . $parentId;
            }

            $categoryId = (int)Mage::app()->getRequest()->getParam('category', false);
            if ($categoryId) {
                $cacheTags[] = 'category';
                $cacheTags[] = 'category_' . $categoryId;
            }
        }
        
        return $cacheTags;
    }

    /**
     * @return array
     */
    protected function _getCatalogCategoryViewCacheTags()
    {
        $cacheTags = array();
        $cacheTags[] = 'category';
        $categoryId = (int)Mage::app()->getRequest()->getParam('id', false);
        
        if ($categoryId) {
            $cacheTags[] = 'category_' . $categoryId;
        }
        
        return $cacheTags;
    }
}