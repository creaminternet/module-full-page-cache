<?php


class Cream_Fpc_Block_Core_Messages extends Mage_Core_Block_Messages
{
    /**
     * Retrieve messages in HTML format grouped by type
     *
     * @param   string $type
     * @return  string
     */
    public function getGroupedHtml()
    {
        $html = parent::getGroupedHtml();

        $_transportObject = new Varien_Object;
        $_transportObject->setHtml($html);
        Mage::dispatchEvent(
            'core_block_messages_get_grouped_html_after',
            array('block' => $this, 'transport' => $_transportObject)
        );
        $html = $_transportObject->getHtml();

        return $html;
    }
}