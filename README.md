# Cream Full Page Cache #

## Installation instructions: ##
1.	Copy “Cream_Fpc.xml”  from the “App\etc\modules” directory to your own Magento installation.
2.	Copy  all content in app\code\community\Cream into  your Magento community folder.
3.	Clear Magento Cache.
4.	Enable Full page caching in cache store Management
## Configuration: ##
The extension configuration is located in System -> Configuration -> Cream -> Full Page Cache. If the configuration tab does not appear, log out and login into your admin panel.

**Cachable actions**

All cachable actions are displayed here, there are four preconfigured actions. These actions enable by default the full page caching for the homepage, cms pages, products pages and category pages. 

* 	Homepage: cms_index_index
* 	CMS pages: cms_page_view
* 	Products pages: catalog_product_view
* 	Category pages: catalog_category_view

Additional actions can be added here, as long as you use the correct layout handle and separate them with a comma.

**Dynamic blocks**

Dynamics blocks are blocks that will not be cached by this extension. These hole punches can be added or removed in this field. You can add your custom blocks here by using the block name as configured in the store XML files. 

**Refresh actions**

Here you can add actions that force the session blocks to refresh. For optimal caching these blocks are being cached until there is need for them to refresh.

**Session Blocks**

Blocks that are stored in the browser session. These blocks will be cached until they are triggered to refresh by a refresh action. Blocks can be added or removed here by block name  as long as you keep them comma separated.

**Category Session Params**

If  category parameters are added here, they will not be cached by this extension. By entering parameters here, category parameter are loaded from the browser session, instead of cached content.
## Testing: ##

In order to test this the Full Page Cache has to be enabled  in cache store Management.
Go to any cachable page which has not been excluded during the configuration step.
Afterwards open the developer console and reload the page, here you can check the response headers when loading the page for the second time.
If you see the line 'Fpc: 1' the module is working correctly, if not there might be an issue with the installation or the configuration.


 
## **FAQ** ##

Q: Parts of my page are cached where I don’t want them to be cached.

A: Figure out if you never want the content to be cached, if so, add the block name to "Dynamic blocks". If you want the block to be cached but refreshed when actions are undertaken, add the block name to session blocks.